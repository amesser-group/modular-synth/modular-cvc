#include "system.hpp"
#include "adsr.hpp"
#include <string.h>

#include "ecpp/Math/FixedPoint.hpp"

using namespace midi2cv;

using namespace ::ecpp::HAL;
using namespace ::ecpp;

static IOPin<AVR_IO_PB0> LED;
static IOPin<AVR_IO_PB3> ADC_MOSI;
static IOPin<AVR_IO_PB5> ADC_SCK;
static IOPin<AVR_IO_PB2> DAC1_CS;
static IOPin<AVR_IO_PD6> DAC1_LAT;
static IOPin<AVR_IO_PD5> DAC2_CS;
static IOPin<AVR_IO_PD4> DAC2_LAT;
static IOPin<AVR_IO_PD7> Button;

static IOPin<AVR_IO_PC1> Gate1_C;
static IOPin<AVR_IO_PD2> Gate1_D;
static IOPin<AVR_IO_PD3> Gate2_G;
static IOPin<AVR_IO_PC0> Gate2_H;

//static IOPin<AVR_IO_PC5> RotarySwitch; /* ADC5 */

#define MIDI_BAUD 31250

static constexpr uint16_t kUBRR_Value = (F_CPU)/16/(MIDI_BAUD);

volatile uint8_t trigger_gate_cnt = 0;

uint8_t ui_divider;

uint8_t led_count   = 0;
uint8_t led_enable_mask = 0;

int8_t button;
uint8_t button_debounce;


enum class ConfigMode : uint8_t
{
  Idle = 0,
  SetBaseNote,
  WaitRelease,
};

ConfigMode config_mode = ConfigMode::Idle;

static constexpr uint16_t kZeroOutput  = 2800;
static constexpr int8_t   kZeroNote    = 48;

static uint_least16_t          dac_counts[4] = {kZeroOutput, kZeroOutput, kZeroOutput, kZeroOutput};
static volatile uint_least8_t  dac_dirty     = true;

static uint8_t cv0_channel   = 0;
static uint8_t cv0_base_note = kZeroNote;
static uint8_t cv0_note      = kZeroNote;

static uint_least8_t rotary_switch = 0;

enum RotarySwitch : uint_least8_t
{
  Default    = 0,
  ArpRising  = 1,
  ArpFalling = 2,
};

typedef ecpp::FixedPoint<int16_t, kZeroOutput * 8> ADSRAmplitude;
typedef ecpp::FixedPoint<uint16_t, 65536UL>          ADSRCoef;

struct ADSRMultiplierTable
{
  template<typename ...ARGS>
  constexpr ADSRMultiplierTable(ARGS... args) : rate_mults_ { static_cast<uint16_t>(args)...} {}

  uint16_t      rate_mults_[128];
};

struct ADSRCurve1_3  { constexpr static ADSRAmplitude kTargetAmplitude {1.3}; };
struct ADSRCurve0_01 { constexpr static ADSRAmplitude kTargetAmplitude {0.01}; };

constexpr ADSRAmplitude ADSRCurve1_3::kTargetAmplitude;
constexpr ADSRAmplitude ADSRCurve0_01::kTargetAmplitude;

template<typename ADSRCurveDefs>
struct ADSRCurve : protected ADSRMultiplierTable, public ADSRCurveDefs
{
public:
  template<typename ...ARGS>
  constexpr ADSRCurve(ARGS... args) : ADSRMultiplierTable {args...}, ADSRCurveDefs{}  {}

  ADSRCoef GetMultiplier(uint8_t idx) const { return ADSRCoef( ::ecpp::MakeFraction(pgm_read_word(&(rate_mults_[idx])), 65536UL)); }
};

static const PROGMEM ADSRCurve<ADSRCurve1_3> kADSRCoef1_3 = 
{
  15124, 55490, 60003, 61719, 62623, 63181, 63559, 63833, 64040,
  64202, 64333, 64440, 64530, 64606, 64671, 64728, 64778, 64822,
  64861, 64896, 64928, 64956, 64982, 65006, 65028, 65048, 65067,
  65084, 65100, 65115, 65129, 65142, 65154, 65166, 65177, 65187,
  65196, 65206, 65214, 65222, 65230, 65238, 65245, 65251, 65258,
  65264, 65270, 65276, 65281, 65286, 65291, 65296, 65300, 65305,
  65309, 65313, 65317, 65321, 65325, 65328, 65332, 65335, 65338,
  65341, 65344, 65347, 65350, 65353, 65356, 65358, 65361, 65363,
  65366, 65368, 65370, 65372, 65375, 65377, 65379, 65381, 65383,
  65385, 65386, 65388, 65390, 65392, 65393, 65395, 65397, 65398,
  65400, 65401, 65403, 65404, 65405, 65407, 65408, 65409, 65411,
  65412, 65413, 65414, 65416, 65417, 65418, 65419, 65420, 65421,
  65422, 65423, 65424, 65425, 65426, 65427, 65428, 65429, 65430,
  65431, 65432, 65433, 65434, 65435, 65435, 65436, 65437, 65438,
  65439, 65439
};

static const PROGMEM ADSRCurve<ADSRCurve0_01> kADSRCoef0_01 = {
  648, 61822, 63640, 64263, 64578, 64768, 64895, 64986, 65054,
  65107, 65150, 65185, 65214, 65239, 65260, 65278, 65294, 65308,
  65321, 65332, 65342, 65352, 65360, 65367, 65374, 65381, 65387,
  65392, 65397, 65402, 65407, 65411, 65415, 65418, 65422, 65425,
  65428, 65431, 65434, 65436, 65439, 65441, 65443, 65446, 65448,
  65450, 65451, 65453, 65455, 65457, 65458, 65460, 65461, 65463,
  65464, 65465, 65466, 65468, 65469, 65470, 65471, 65472, 65473,
  65474, 65475, 65476, 65477, 65478, 65479, 65479, 65480, 65481,
  65482, 65482, 65483, 65484, 65485, 65485, 65486, 65487, 65487,
  65488, 65488, 65489, 65489, 65490, 65491, 65491, 65492, 65492,
  65493, 65493, 65493, 65494, 65494, 65495, 65495, 65496, 65496,
  65496, 65497, 65497, 65498, 65498, 65498, 65499, 65499, 65499,
  65500, 65500, 65500, 65501, 65501, 65501, 65502, 65502, 65502,
  65502, 65503, 65503, 65503, 65504, 65504, 65504, 65504, 65505,
  65505, 65505
};

typedef midi2cv::ADSREnvelope<ADSRAmplitude, ADSRCoef> Envelope;

static Envelope adsr0;

uint_least16_t arp_ticks = 0;

/** this runs with a 1khz Frequency */
ISR(TIMER0_OVF_vect)
{
  TCNT0 = 256 - 250;

  if(arp_ticks > 0)
    --arp_ticks;

  if(ui_divider < 99)
  {
    ui_divider +=1;
  }
  else
  {
    ui_divider = 0;

    if(led_count > 0)
    {
      LED = (led_count & led_enable_mask) != 0;
      led_count -= 1;
    }
    else
    {
      LED = false;
    }

    if((button > 0) && !Button)
    {
      if(button < 127)
        button += 1;
    }
  }

  if(button_debounce > 0)
  {
    button_debounce-= 1;
  }
  else
  {
    if ((button == 0) && !Button)
    {
      button_debounce = 20;
      button = 1;
    }
    else if ((button > 0) && Button)
    {
      button_debounce = 20;
      button = -button;
    }
  }

  Gate1_D = (adsr0.Step(Gate1_C.getOutput()) != 0);
}

void write_adc_register(uint8_t command, uint16_t value)
{
  SPDR = command; while((SPSR & _BV(SPIF)) == 0);
  SPDR = (value >> 8) & 0xFF; while((SPSR & _BV(SPIF)) == 0);
  SPDR = (value >> 0) & 0xFF; while((SPSR & _BV(SPIF)) == 0);
}


void apply_voltages()
{
  dac_dirty = false;

  DAC1_CS  = false;
  write_adc_register(0x01 << 3, dac_counts[0]);
  DAC1_CS  = true;

  DAC1_CS  = false;
  write_adc_register(0x00 << 3, dac_counts[1]);
  DAC1_CS  = true;

  DAC2_CS  = false;
  write_adc_register(0x01 << 3, dac_counts[2]);
  DAC2_CS  = true;

  DAC2_CS  = false;
  write_adc_register(0x00 << 3, dac_counts[3]);
  DAC2_CS  = true;

  DAC1_LAT = false;
  DAC2_LAT = false;
  _delay_us(1);
  DAC1_LAT = true;
  DAC2_LAT = true;

  /* set gate after setting DAC voltage */
  if (cv0_note & 0x80)
    Gate1_C = true;
}

using ::midi2cv::midi_processor;



static int8_t cv0_pitch_bend = 0;

void set_note_voltage()
{
  static constexpr int8_t  kMinNote     = -3 * 12;
  static constexpr int8_t  kMaxNote     =  7 * 12;
  
  int8_t note = (cv0_note & 0x7F) - cv0_base_note;

  if(note < kMinNote) note = kMinNote;
  else if(note > kMaxNote) note = kMaxNote;

  uint16_t counts = static_cast<uint16_t>(kZeroOutput - (note * 32) - cv0_pitch_bend);

  dac_counts[0] = counts;
}


void do_note_on_cv0(uint8_t note, uint8_t velocity)
{
  cv0_note  = note | 0x80;

  Gate1_C   = false;
  dac_dirty = true;
  adsr0.ReTrigger();
}

void do_note_off_cv0(uint8_t note, uint8_t velocity)
{
  if(cv0_note == (note | 0x80))
  {
    cv0_note = note;
    Gate1_C  = false;
  }
}

class ArpProcessor
{
public:
  enum Direction : int_least8_t { Upwards = -1, Downwards = 1};
  void Reset();

  void NoteOn(uint8_t note);
  void NoteOff(uint8_t note);

  uint_least8_t GetNextNote();

  void SetDirection(Direction dir) { mode_ = dir;}
  void SetOctaves(int_least8_t oct) {octaves_ = oct;}

  constexpr bool idle() { return state_ == 0;}

protected:
  uint_least8_t ComputeNextNote(uint_least8_t entry);

  static constexpr uint_least8_t kMaxNoteCount = 8;

  uint_least8_t notes_[kMaxNoteCount+1];
  uint_least8_t current_note_;
  
  /* state < 0 -> upwards */
  /* state > 0 -> downwards */
  int_least8_t  state_;
  int_least8_t  mode_;

  int_least8_t  octaves_;
};

void
ArpProcessor::Reset()
{
  memset(notes_, 0x00, sizeof(notes_));
}

void
ArpProcessor::NoteOn(uint8_t note)
{
  uint_least8_t i;

  for(i = 0; i < kMaxNoteCount; ++i)
  {
    if(note > notes_[i])
    {
      if(current_note_ >= i)
        current_note_ = (current_note_ + 1) % kMaxNoteCount;

      break;
    }
  }

  for(;i < kMaxNoteCount; ++i)
  {
    auto bak = notes_[i];

    notes_[i] = note;
    note = bak;
  }
}

void
ArpProcessor::NoteOff(uint8_t note)
{
  uint_least8_t i;

  for(i = 0; i < kMaxNoteCount; ++i)
  {
    if(notes_[i] == note)
    {
      if(current_note_ >= i)
        current_note_ = (current_note_ - 1 + kMaxNoteCount) % kMaxNoteCount;
      break;
    }
  }

  for(; i < kMaxNoteCount; ++i)
  {
    notes_[i] = notes_[i+1];
  }
}

uint_least8_t
ArpProcessor::ComputeNextNote(uint_least8_t entry)
{
  uint_least8_t note = notes_[entry];

  if(mode_ >= Downwards)
  {
    if(state_ >= Downwards)
    {
      if (entry <= current_note_)
        state_ += 12;

      if(state_ > (octaves_ + Downwards))
        state_ = Downwards;
    }
    else
    {
      state_ = Downwards;
    }

    int_least8_t transpose = state_ - Downwards;

    if (note > transpose)
      note = note-transpose;
    else
      note = 1;
  }
  else
  {
    if(state_ <= Upwards)
    {
      if (entry >= current_note_)
        state_ -= 12;

      if(state_ < (octaves_ - Upwards))
        state_ = Upwards;
    }
    else
    {
      state_ = Upwards;
    }

    int_least8_t transpose = state_ - Upwards ;

    if ( note < (transpose + 127))
      note = note-transpose;
    else
      note = 127;

  }

  current_note_ = entry;
  return note;
}

uint_least8_t
ArpProcessor::GetNextNote()
{
  uint_least8_t entry = current_note_;
  uint_least8_t i;

  if(state_ == 0)
  {
    if(mode_ < 0)
      current_note_ = entry = 0;
    else
      current_note_ = entry = 7;
  }

  for(i = 0; i < kMaxNoteCount; ++i)
  {
    entry = (entry + mode_ + kMaxNoteCount) % kMaxNoteCount;

    if(notes_[entry])
      return ComputeNextNote(entry);
  }

  state_ = 0;

  return 0;
}

static ArpProcessor arp_processor;
uint_least16_t      arp_delay = 0;

void do_note_on(uint8_t note, uint8_t velocity)
{
  dac_counts[2] = kZeroOutput - static_cast<uint16_t>(16) * velocity;
  dac_dirty = true;

  switch(rotary_switch)
  {
  case RotarySwitch::ArpRising:
  case RotarySwitch::ArpFalling:
    /* delay first node by 20ms when arp is idle to avoid 
     * unexpected arp order */

    if (arp_processor.idle() && (arp_ticks == 0))
      arp_ticks = 20;

    arp_processor.NoteOn(note);
    break;
  default:
    do_note_on_cv0(note, velocity);
    break;
  }
}

void do_note_off(uint8_t note, uint8_t velocity)
{
  switch(rotary_switch)
  {
  case RotarySwitch::ArpRising:
  case RotarySwitch::ArpFalling:
    arp_processor.NoteOff(note);
    break;
  default:
    do_note_off_cv0(note, velocity);
    break;
  }
}


void process_note_off_msg(uint8_t midi_status)
{
  const int8_t note     = static_cast<int8_t>(midi_processor.GetNextByte());
  const int8_t velocity = static_cast<int8_t>(midi_processor.GetNextByte());

  if((midi_status & 0x0F) == cv0_channel)
    do_note_off(note,velocity);
}

void process_note_on_msg(uint8_t midi_status)
{
  const int8_t note     = static_cast<int8_t>(midi_processor.GetNextByte());
  const int8_t velocity = static_cast<int8_t>(midi_processor.GetNextByte());

  if(config_mode == ConfigMode::SetBaseNote)
  {
    cv0_channel   = midi_status & 0x0F;
    cv0_base_note = note;
    config_mode   = ConfigMode::WaitRelease;
  }

  if((midi_status & 0x0F) == cv0_channel)
  {
    if(velocity > 0)
    {
      do_note_on(note,velocity);
    }
    else
    {
      do_note_off(note, velocity);
    }      
  }
}

void process_pitch_bend_msg(uint8_t midi_status)
{
  const uint8_t lsb = static_cast<uint8_t>(midi_processor.GetNextByte());
  const int8_t  msb = static_cast<int8_t>(midi_processor.GetNextByte());

  if((midi_status & 0x0F) == cv0_channel)
  {
    cv0_pitch_bend = msb - Midi::kPitchBendCenter;
    dac_dirty = true;
  }
}


static constexpr uint8_t kMidiControllerArpOctaves  = 16;

static constexpr uint8_t kMidiControllerReleaseTime = 72;
static constexpr uint8_t kMidiControllerAttackTime  = 73;

static constexpr uint8_t kMidiControllerDecayTime   = 75;
static constexpr uint8_t kMidiControllerSustain     = 79;


void process_control_change(uint8_t midi_status)
{
  const uint8_t controller = midi_processor.GetNextByte();
  const uint8_t value      = midi_processor.GetNextByte();

  if((midi_status & 0x0F) == cv0_channel)
  {
    switch(controller)
    {
    case kMidiControllerArpOctaves:
      arp_processor.SetOctaves(static_cast<int8_t>(value - 0x40));
      break;
    case kMidiControllerReleaseTime: 
      adsr0.SetRelease(0x7F - value, kADSRCoef0_01);  
      break;
    case kMidiControllerAttackTime:  
      adsr0.SetAttack(0x7F - value, kADSRCoef1_3);   
      break;    
    case kMidiControllerDecayTime:   
      adsr0.SetDecay(0x7F - value, kADSRCoef0_01);   
      break;
    
    case kMidiControllerSustain:     
      adsr0.SetSustain(::ecpp::MakeFraction(value, 0x7F), kADSRCoef0_01);   
      break;
    default: 
      break;
    }
  }
}

static uint_least8_t adc_sample_state = 0;

void start_adc(uint8_t state)
{
  ADMUX  = adc_sample_state = state;
  ADCSRA = _BV(ADEN) | _BV(ADSC) | _BV(ADIF) |
           _BV(ADPS2) | _BV(ADPS1) | _BV(ADPS0);
}

void handle_adc()
{
  switch(adc_sample_state)
  {
  case _BV(REFS0) | 4:
    if(ADCSRA & _BV(ADIF))
    {
      uint16_t val = ADC;

      switch(rotary_switch)
      {
      case ArpRising:
      case ArpFalling:
        arp_delay = 0x1FF - (val / 2); 
        break;
      default: break;
      }
      start_adc(_BV(REFS0) | 5);
    }
    break;
  case _BV(REFS0) | 5:
    if(ADCSRA & _BV(ADIF))
    {
      uint16_t val = ADC;

      val = (val + 46) / 92;

      if(val != rotary_switch)
      {
        rotary_switch = val;

        led_enable_mask = 0x2;
        led_count       = 0x2*(val+1)*2-1;

        arp_processor.Reset();
      }
      start_adc(_BV(REFS0) | 4);
    }
    break;
  default:
    start_adc(_BV(REFS0) | 4);
    break;
  }
}

void handle_arp()
{
  const auto arp_interval = arp_delay + 10;
  uint8_t note = 0;

  if (rotary_switch == RotarySwitch::ArpRising)
    arp_processor.SetDirection(arp_processor.Upwards);
  else if (rotary_switch == RotarySwitch::ArpFalling)
    arp_processor.SetDirection(arp_processor.Downwards);
  else
    return;

  if (arp_ticks == 0)
  {
    note = arp_processor.GetNextNote();

    if(note > 0)
    {
      do_note_on_cv0(note, 64);
      arp_ticks = arp_interval;
    }
  }
  else if(arp_ticks < (arp_interval/2))
  {
    do_note_off_cv0(cv0_note&0x7F, 0);
  }
}


int16_t last_adsr0_amplitude = -1;

void handle_dac()
{
  volatile Envelope &env = adsr0;
  auto amplitude = env.amplitude();

  while(amplitude != env.amplitude())
    amplitude = env.amplitude();

  uint_least16_t dac3_counts = kZeroOutput - (amplitude.raw().value / 8);

  if(dac3_counts != dac_counts[3])
  {
    dac_counts[3] = dac3_counts;
    dac_dirty = true;
  }

  if (dac_dirty)
  {
    set_note_voltage();
    apply_voltages();
  }
}

int main()
{

  LED      = true;
  DAC1_CS  = true;
  DAC1_LAT = true;
  DAC2_CS  = true;
  DAC2_LAT = true;
  Button   = true; /* Enable Pull-Up for Button */

  Gate2_G = Gate1_C = false;
  Gate2_H = Gate1_D = false;

  LED.enableOutput();

  DAC1_CS.enableOutput();
  DAC1_LAT.enableOutput();  
  DAC2_CS.enableOutput();
  DAC2_LAT.enableOutput();  

  ADC_MOSI.enableOutput();
  ADC_SCK.enableOutput();

  Gate1_C.enableOutput();
  Gate1_D.enableOutput();
  Gate2_G.enableOutput();
  Gate2_H.enableOutput();

  SPCR = _BV(SPE) | _BV(MSTR) | _BV(SPR0);

  UBRRH = (kUBRR_Value >> 8) & 0x7F;
  UBRRL = (kUBRR_Value >> 0) & 0xFF;

  UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0);

  UCSRB = _BV(RXCIE) | _BV(RXEN);


  DAC1_CS = false;
  write_adc_register(0x08 << 3, 0x000A); /* use VREF unbuffered */
  DAC1_CS = true;

  DAC2_CS = false;
  write_adc_register(0x08 << 3, 0x000A); /* use VREF unbuffered */
  DAC2_CS = true;

  adsr0.SetAttack(0x40, kADSRCoef1_3);   
  adsr0.SetRelease(0x40, kADSRCoef0_01);  
  adsr0.SetDecay(0x40, kADSRCoef0_01);   
  adsr0.SetSustain(0.5, kADSRCoef0_01);   

  TCNT0 = 256 - 250;
  TIMSK = _BV(TOIE0);

  ::ecpp::Sys_AVR8::enableInterrupts();

  TCCR0 = _BV(CS01) | _BV(CS00);

  while(1)
  {
    if(midi_processor.GetByteCount() > 0)
    {
      uint8_t midi_status = midi_processor.GetNextByte();

      switch(midi_status & 0xF0)
      {
      case Midi::kStatusNoteOff: 
        process_note_off_msg(midi_status); 
        break;
      case Midi::kStatusNoteOn:  
        process_note_on_msg(midi_status); 
        break;
      case Midi::kStatusControlChange:
        process_control_change(midi_status);
        break;
      case Midi::kStatusPitchBend:
        process_pitch_bend_msg(midi_status);
        break;
      default: 
        break;
      }
    }

    if(button > 0)
    {
      switch(config_mode)
      {
      case ConfigMode::Idle:
        if(button > 20)
        {
          config_mode = ConfigMode::SetBaseNote;
          led_enable_mask = 0x4;
          led_count       = 0x4*3;
        }
        break;
      }
    }
    else if(button < 0)
    {
      button = 0;
    }
    else
    {
      if(config_mode == ConfigMode::WaitRelease)
        config_mode = ConfigMode::Idle;
    }

    handle_arp();
    handle_dac();
    handle_adc();
  }
}
