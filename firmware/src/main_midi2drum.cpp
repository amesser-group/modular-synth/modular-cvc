#include "ecpp/Sound/Midi/GeneralMidi.hpp"
#include "system.hpp"

namespace GeneralMidi = ecpp::Sound::Midi::GeneralMidi;

using namespace midi2cv;
using namespace ::ecpp::HAL;
using namespace ::ecpp;

static IOPin<AVR_IO_PD6> Gate1_A;
static IOPin<AVR_IO_PB2> Gate1_B;
static IOPin<AVR_IO_PC1> Gate1_C;
static IOPin<AVR_IO_PD2> Gate1_D;
static IOPin<AVR_IO_PD4> Gate2_E;
static IOPin<AVR_IO_PD5> Gate2_F;
static IOPin<AVR_IO_PD3> Gate2_G;
static IOPin<AVR_IO_PC0> Gate2_H;

static uint_least8_t accent = false;

#define MIDI_BAUD 31250

static constexpr uint16_t kUBRR_Value = (F_CPU)/16/(MIDI_BAUD);


class DrumTrigger
{
public:
  constexpr bool active() { return (count_ > kReTriggerPause) && count_ <= (kTriggerLength+kReTriggerPause); }
  void tick() { if(count_ > 0) --count_;}

  void Trigger() {count_ = kTriggerLength+kReTriggerPause;}
  void Retrigger();

  static constexpr uint_least8_t kTriggerLength  = 20;
  static constexpr uint_least8_t kReTriggerPause =  1;
private:
  uint_least8_t count_ = 0;
};

void
DrumTrigger::Retrigger()
{ 
  uint_least8_t c = count_;

  if(c == 0)
    Trigger();
  else if (c <= kReTriggerPause)
    count_ = c + kTriggerLength;
  else if (c <= kTriggerLength+kReTriggerPause)
    count_ = kTriggerLength+2*kReTriggerPause;
}

static DrumTrigger triggers[8+1];

/* 1ms timer interrupt */
ISR(TIMER0_OVF_vect)
{
  TCNT0 = 256 - 250;

  /** accent overrides output 7 to become accent pin */
  
  Gate1_A = triggers[0].active();
  Gate1_B = triggers[1].active();
  Gate1_C = triggers[2].active();
  Gate1_D = triggers[3].active();
  Gate2_E = triggers[4].active();
  Gate2_F = triggers[5].active();
  Gate2_G = triggers[6].active();
  Gate2_H = accent or triggers[7].active();

  triggers[0].tick();
  triggers[1].tick();
  triggers[2].tick();
  triggers[3].tick();
  triggers[4].tick();
  triggers[5].tick();
  triggers[6].tick();
  triggers[7].tick();
}

using ::midi2cv::midi_processor;



DrumTrigger &
GetTrigger(int8_t note)
{
  switch(note)
  {
  case GeneralMidi::Percussion::Note::kElectricBassDrum : return triggers[0];
  case GeneralMidi::Percussion::Note::kHandClap:         return triggers[1];
  case GeneralMidi::Percussion::Note::kElectricSnare:    return triggers[2];
  case GeneralMidi::Percussion::Note::kClosedHiHat:      return triggers[3];
  case GeneralMidi::Percussion::Note::kOpenHiHat:        return triggers[4];
  case GeneralMidi::Percussion::Note::kCowBell:          return triggers[5];
  case GeneralMidi::Percussion::Note::kMaracas:          return triggers[6];
  case GeneralMidi::Percussion::Note::kClaves:           return triggers[7];
  default:                    return triggers[8];
  }
}

void do_note_off(uint8_t note, uint8_t velocity)
{

}

void process_note_off_msg(uint8_t midi_status)
{
  const int8_t note     = static_cast<int8_t>(midi_processor.GetNextByte());
  const int8_t velocity = static_cast<int8_t>(midi_processor.GetNextByte());

  if((midi_status & 0x0F) == GeneralMidi::Percussion::kChannel)
    do_note_off(note,velocity);
}

static uint8_t velocity_accent_threshold = 64;

void process_note_on_msg(uint8_t midi_status)
{
  const int8_t note     = static_cast<int8_t>(midi_processor.GetNextByte());
  const int8_t velocity = static_cast<int8_t>(midi_processor.GetNextByte());

  if(velocity > 0)
  {
    if((midi_status & 0x0F) == GeneralMidi::Percussion::kChannel)
    {
      /* accent is gobally set on last notes velocity */
      accent = (velocity > velocity_accent_threshold);

      GetTrigger(note).Retrigger();
    }
  }
  else
  {
    do_note_off(note, 0);
  }
}

static constexpr uint8_t kMidiControllerExpression = 11;


void process_control_change(uint8_t midi_status)
{
  const int8_t controller = static_cast<int8_t>(midi_processor.GetNextByte());
  const int8_t value = static_cast<int8_t>(midi_processor.GetNextByte());

  if((midi_status & 0x0F) == GeneralMidi::Percussion::kChannel)
  {
    switch(controller)
    {
    case kMidiControllerExpression: velocity_accent_threshold = value;
    default: break;
    }
  }
}


int main()
{

  Gate1_A = Gate1_B = Gate1_C = Gate1_D = false;
  Gate2_E = Gate2_F = Gate2_G = Gate2_H = false;

  Gate1_A.enableOutput();
  Gate1_B.enableOutput();
  Gate1_C.enableOutput();
  Gate1_D.enableOutput();
  Gate2_E.enableOutput();
  Gate2_F.enableOutput();
  Gate2_G.enableOutput();
  Gate2_H.enableOutput();

  UBRRH = (kUBRR_Value >> 8) & 0x7F;
  UBRRL = (kUBRR_Value >> 0) & 0xFF;

  UCSRC = _BV(URSEL) | _BV(UCSZ1) | _BV(UCSZ0);

  UCSRB = _BV(RXCIE) | _BV(RXEN);

  TCNT0 = 256 - 250;
  TIMSK = _BV(TOIE0);

  ::ecpp::Sys_AVR8::enableInterrupts();

  TCCR0 = _BV(CS01) | _BV(CS00);

  while(1)
  {
    if(midi_processor.GetByteCount() > 0)
    {
      uint8_t midi_status = midi_processor.GetNextByte();

      switch(midi_status & 0xF0)
      {
      case Midi::kStatusNoteOff: 
        process_note_off_msg(midi_status); 
        break;
      case Midi::kStatusNoteOn:  
        process_note_on_msg(midi_status); 
        break;
      case Midi::kStatusControlChange:
        process_control_change(midi_status);
        break;
      default: 
        break;
      }
    }
  }
}
