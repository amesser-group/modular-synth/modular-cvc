
#ifndef MIDI2CV_SYSTEM_HPP_
#define MIDI2CV_SYSTEM_HPP_

#include "ecpp/HAL/AVR8.hpp"

#include "ecpp/Sound/Midi/Definitions.hpp"
#include "ecpp/Sound/Midi/Receiver.hpp"

namespace midi2cv
{
  namespace Midi = ecpp::Sound::Midi;

  using MidiReceiver = Midi::Receiver<Midi::RingBuffer<16>>;

  extern MidiReceiver midi_processor;
};

#endif