#include "ecpp/HAL/AVR8.hpp"
#include "system.hpp"

namespace midi2cv {
    MidiReceiver midi_processor;
}

ISR(USART_RXC_vect)
{
  uint8_t rx_data = UDR;
    
  ::midi2cv::midi_processor.ProcessCharacter(rx_data);  
}