/*
 *  Copyright 2022 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the MIDI-2-CV firmware
 *
 *  MIDI-2-CV firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  MIDI-2-CV firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *  */
#ifndef MIDI2CV_ADSR_HPP_
#define MIDI2CV_ADSR_HPP_

#include <cstdint>

namespace midi2cv
{
  /** ADSR envelope algorithm based on work from  Ear Level Engineering:
   *  https://www.earlevel.com/main/2013/06/02/envelope-generators-adsr-part-2/ */

  template<typename AmplitudeType, typename MultiplierType>
  class StageParameter
  {
  public:
    constexpr StageParameter() {};
    constexpr StageParameter(const MultiplierType & mult, const AmplitudeType & base) : mult_(mult), base_(base) {};

    AmplitudeType CalculateNextStep(AmplitudeType current) const;

    void Setup(MultiplierType mult, AmplitudeType target);

    constexpr const MultiplierType & mult() { return mult_; };

  protected:
    /** fixed point arithmetics, this value here is 0-1 16 bit denominator */
    MultiplierType mult_ {0};
    AmplitudeType  base_ {0};
  };

  template<typename AmplitudeType, typename MultiplierType>
  AmplitudeType    StageParameter<AmplitudeType, MultiplierType>::CalculateNextStep(AmplitudeType current) const
  {
    return base_ + current * mult_;
  }

  template<typename AmplitudeType, typename MultiplierType>
  void StageParameter<AmplitudeType, MultiplierType>::Setup(MultiplierType mult, AmplitudeType target) 
  {  
    base_ = target - target*mult;  
    mult_ = mult;
  }

  template<typename A, typename M >
  class ADSREnvelope
  {
  public:
    using AmplitudeType  = A;
    using MultiplierType = M;

    using Stage          = StageParameter<AmplitudeType, MultiplierType>;

    constexpr ADSREnvelope() {}

    void      ReTrigger() { attacked_ = false; }
    
    uint_fast8_t Step(uint8_t note_on);

    AmplitudeType amplitude() volatile { return AmplitudeType(value_); } 

    template<typename CoefficientTable>
    void SetAttack(uint8_t idx,  const CoefficientTable& attack_coef);

    template<typename CoefficientTable>
    void SetDecay(uint8_t  idx,  const CoefficientTable& decay_coef);

    template<typename CoefficientTable>
    void SetRelease(uint8_t idx, const CoefficientTable& decay_release);

    template<typename CoefficientTable>
    void SetSustain(AmplitudeType amp, const CoefficientTable& decay_coef);

  protected:
    AmplitudeType          value_     = 0;
    AmplitudeType          sustain_   = 0;
    uint_least8_t          attacked_  = 0;

    Stage                  attack_  = {};
    Stage                  decay_   = {};
    Stage                  release_ = {};
  };

  template<typename AmplitudeType, typename MultiplierType>
  uint_fast8_t
  ADSREnvelope<AmplitudeType, MultiplierType>::Step(uint8_t gate)
  {
    AmplitudeType new_value = value_;
    uint8_t attacking = false;

    if(gate)
    {
      if(!attacked_)
      {
        new_value = attack_.CalculateNextStep(new_value);

        if(new_value > 1)
        {
          new_value  = 1;
          attacked_ = true;
        }

        attacking = true;
      }
      else if(new_value != sustain_)
      {
        new_value = decay_.CalculateNextStep(new_value);

        if(new_value <= sustain_)
          new_value = sustain_;
      }

      value_ = new_value;
    }
    else 
    {
      if (new_value > 0)
      {
        new_value = release_.CalculateNextStep(new_value);

        if(new_value < 0)
          new_value = 0;

        value_ = new_value;
      }

      attacked_ = false;
    }

    return attacking;
  }

  template<typename AmplitudeType, typename MultiplierType>
  template<typename CoefficientTable>
  void ADSREnvelope<AmplitudeType, MultiplierType>::SetAttack(uint8_t idx,  const CoefficientTable& attack_coef)
  {
    attack_.Setup(attack_coef.GetMultiplier(idx), attack_coef.kTargetAmplitude);
  }

  template<typename AmplitudeType, typename MultiplierType>
  template<typename CoefficientTable>
  void ADSREnvelope<AmplitudeType, MultiplierType>::SetDecay(uint8_t  idx,  const CoefficientTable& decay_coef)
  {
    decay_.Setup(decay_coef.GetMultiplier(idx), sustain_ - decay_coef.kTargetAmplitude);
  }

  template<typename AmplitudeType, typename MultiplierType>
  template<typename CoefficientTable>
  void ADSREnvelope<AmplitudeType, MultiplierType>::SetRelease(uint8_t idx, const CoefficientTable& release_coef)
  {
    release_.Setup(release_coef.GetMultiplier(idx), - release_coef.kTargetAmplitude);
  }

  template<typename AmplitudeType, typename MultiplierType>
  template<typename CoefficientTable>
  void ADSREnvelope<AmplitudeType, MultiplierType>::SetSustain(AmplitudeType amp, const CoefficientTable& decay_coef)
  {
    decay_.Setup(decay_.mult(), amp - decay_coef.kTargetAmplitude);
    sustain_ = amp;
  }
}

#endif