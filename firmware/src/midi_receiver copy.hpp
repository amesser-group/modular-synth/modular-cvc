/*
 *  Copyright 2022 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the MIDI-2-CV firmware
 *
 *  MIDI-2-CV firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  MIDI-2-CV firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *  */
#ifndef MIDI2CV_MIDI_RECEIVER_HPP_
#define MIDI2CV_MIDI_RECEIVER_HPP_

#include <cstdint>
#include "ecpp/Ringbuffer.hpp"

namespace midi2cv
{
  class MidiReceiver
  {
  public:
    uint8_t GetNextByte()      {return rx_ring_.pop();}
    
    auto    GetByteCount()     {return rx_ring_.getCount();}

    void ProcessCharacter(uint8_t rx_data)
    {
      if (rx_data & 0x80)
      { /* this is a status byte */
        if ((rx_data & 0xF8) == 0xF8)
        { /* real-time messages can interrupt anything
          * skip them for now */
    
        }
        else
        {
          running_status_ = rx_data;
          third_byte_ = false;
        }
      }
      else if (third_byte_)
      {
        third_byte_ = false;
        rx_ring_.write(2, rx_data);
        rx_ring_.commit(3);
      }
      else if (running_status_)
      {
        if(running_status_ < 0xC0)
          third_byte_ = true;
        else if((running_status_ >= 0xE0) && (running_status_ < 0xF0))
          third_byte_ = true;
        else if(running_status_ == 0xF2)
          third_byte_ = true;
    
        if((running_status_ > 0xF3) ||
          (running_status_ == 0xF0))
        {
          running_status_ = 0;
        }
        else if(third_byte_)
        {
          if(rx_ring_.space() >= 3)
          {
            rx_ring_.write(0, running_status_);
            rx_ring_.write(1, rx_data);
          }
          else
          { /* ring overrun, wait for next status */
            running_status_ = 0;
            third_byte_     = false;
          }
        }
        else
        {
          if(rx_ring_.space() >= 2)
          {
            rx_ring_.write(0, running_status_);
            rx_ring_.write(1, rx_data);
          }
          else
          { /* ring overrun, wait for next status */
            running_status_ = 0;
            third_byte_     = false;
          }
        }
    
        if(running_status_ >= 0xF0)
          running_status_ = 0;
      }
    }
  private:
    ::ecpp::Ringbuffer<uint8_t, 16> rx_ring_;
    uint8_t                         running_status_ = 0;
    uint8_t                         third_byte_     = 0;
  };

};

#endif
