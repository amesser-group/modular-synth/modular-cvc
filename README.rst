#################################################
Eurorack Modular Synth Control Voltage Controller
#################################################

Overview
========

This project is about creating a control voltage thing for use
with `Döpfer A-100`_ Eurorack systems. The main purpose of the PCB
is to serve as Midi to CV interface. But It could be also used
as a base for other purposes like Quantizers, Software LFOs, 
Sequencers. The interface uses 12 Bit DACs with manual trimming
of each output to provide full 12 Bit resolution. The CV output range
covers at least -2.5V to +7V

.. image:: doc/images/controller-variants-front.jpg

Depending on the case / frontplate design, the board can be cut into
two pieces to be stacked on each other.

.. _Döpfer A-100: https://doepfer.de/home_d.htm

Status
======

Schematic & pcb design are ready. A first revision has been
corrected to produce accurate CV voltages independend of load
on the CV outputs. 

I have built a midi to cv interface using two base boards 
using midi circuitry assembled on a prototyping board. The
software is currently work in progress an will be extended
as needed.

.. image:: doc/images/midi2cv-front.jpg


Licensing
=========

All files of the project are licensed under the conditions of 
CC BY-SA license. Software files are covered by GPL v3 if
not otherwise noted in the files


Copyright
=========

If not otherwise noted within the files, the copyright for 
all files in this project folder is
(c) 2022 Andreas Messer <andi@bastelmap.de>.

Links
=====

.. target-notes::


