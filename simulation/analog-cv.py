#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt

adc_counts = np.arange(0,4095)

def cv_target(adc_counts):
    return 7.0 - (adc_counts - 112.) / 384.


r3_base=36e3
r1_base=9.1e3

def cv_circuit(adc_counts, r_slope=2.5e3, r_offset=2.5e3, tol = 0.0, tol_off = 0.0):
    # u_out = u_off * r4 / (r2+r4)*(r1+r3)/r1 - u_adc*r3/r1
    u_adc = 2.5 * adc_counts / 4096
    u_ref = 2.5

    r3 = r3_base*(1+tol) + r_slope
    r1 = r1_base*(1-tol)

    r2 = 9.1e3 *(1+tol_off)
    r4 = 9.1e3 *(1-tol_off) + r_offset

    return u_ref*(r4)/(r4+r2)*(1+r3/r1) - u_adc * (r3/r1)




def calc_r_slope(tol):
    return 4096 / 2.5 / 384 * (r1_base * (1-tol)) - r3_base*(1+tol)


fig, (ax) = plt.subplots()

ax.plot(adc_counts, cv_target(adc_counts), '-b', label='Target curve')

tol = 0.01
r_slope = calc_r_slope(tol)

ax.fill_between(
    adc_counts, 
    cv_circuit(adc_counts, r_slope=r_slope, r_offset=0,tol=tol, tol_off=0.01),
    cv_circuit(adc_counts, r_slope=r_slope, r_offset=5e3,tol=tol, tol_off=0.01),
    color='tab:orange',
    alpha=0.2,
    label=f'Tolerance +1%, Rslope = {r_slope:.0f} Ohm'
)

tol = -0.01
r_slope = calc_r_slope(tol)

ax.fill_between(
    adc_counts, 
    cv_circuit(adc_counts, r_slope=r_slope, r_offset=0,tol=tol, tol_off=-0.01),
    cv_circuit(adc_counts, r_slope=r_slope, r_offset=5e3,tol=tol, tol_off=-0.01),
    color='tab:green',
    alpha=0.2,
    label=f'Tolerance -1%, Rslope = {r_slope:.0f} Ohm'
)


ax.legend(loc='lower left')

plt.show()